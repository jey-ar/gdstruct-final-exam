#pragma once
#include <assert.h>

template<class T>
class Stack {
public:
	Stack(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) {
		if (size) {
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = (growBy > 0 ? growBy : 0);
		}
	}

	virtual ~Stack() {
		if (mArray != NULL) {
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) {
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize) expand();

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual void pop_back() {
		if (mNumElements > 0) mNumElements--;
	}

	virtual T& operator[](int index) {
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual T& top() {
		assert(mArray != NULL && mNumElements > 0);
		return mArray[mNumElements - 1];
	}

	virtual int getNumElements() {
		return mNumElements;
	}

private:
	T* mArray;
	int mGrowSize, mMaxSize, mNumElements;

	bool expand() {
		if (mGrowSize <= 0) return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};