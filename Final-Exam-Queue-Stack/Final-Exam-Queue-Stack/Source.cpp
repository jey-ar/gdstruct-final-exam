#include <iostream>
#include "Stack.h"
#include "Queue.h"

using namespace std;

void PrintTopThenRemove(Stack<int> & stack) {
	while (stack.getNumElements() > 0) {
		cout << stack.top() << " ";
		stack.pop_back();
	}
}

void PrintTopThenRemove(Queue<int> & stack) {
	while (stack.getNumElements() > 0) {
		cout << stack.top() << " ";
		stack.pop_front();
	}
}

int main() {
	cout << "Enter size of elements: "; int elemSize; cin >> elemSize;

	Stack<int> stack(elemSize);
	Queue<int> queue(elemSize);

	while (true) {
		system("cls");

		cout << "Stack: ";
		cout << "\nQueue: ";

		cout << "\n"
			<< "\n[0] - Push Element"
			<< "\n[1] - Pop Top Element"
			<< "\n[2] - Print All Elements and Remove"
			<< endl;

		cout << "Input: "; int choice; cin >> choice;
		int value;

		switch (choice) {
		case 0:
			cout << "Enter value to push: "; cin >> value;

			stack.push(value);
			queue.push(value);
			break;

		case 1:
			cout << "Element Removed: " << endl;
			if (stack.getNumElements() > 0) cout << "\tStack: " << stack.top() << endl;
			if (queue.getNumElements() > 0) cout << "\tQueue: " << queue.top() << endl;

			stack.pop_back();
			queue.pop_front();
			break;

		case 2:
			cout << "All Elements Removed." << endl;

			cout << "Stack: "; PrintTopThenRemove(stack);
			cout << "\nQueue: "; PrintTopThenRemove(queue);
			cout << endl;
			break;

		default:
			break;
		}
		system("pause");
	}
	return 0;
}